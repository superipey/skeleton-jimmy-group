package id.co.dapenbi.core.model;

public class Privilege {
	private boolean flagRead;
	private boolean flagEdit;
	private boolean flagDelete;

	public boolean isFlagRead() {
		return flagRead;
	}

	public void setFlagRead(boolean flagRead) {
		this.flagRead = flagRead;
	}

	public boolean isFlagEdit() {
		return flagEdit;
	}

	public void setFlagEdit(boolean flagEdit) {
		this.flagEdit = flagEdit;
	}

	public boolean isFlagDelete() {
		return flagDelete;
	}

	public void setFlagDelete(boolean flagDelete) {
		this.flagDelete = flagDelete;
	}

}
