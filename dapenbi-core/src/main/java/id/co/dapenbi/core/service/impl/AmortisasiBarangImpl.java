package id.co.dapenbi.core.service.impl;

import id.co.dapenbi.core.model.AmortisasiBarang;
import id.co.dapenbi.core.repository.AmortisasiBarangRepository;
import id.co.dapenbi.core.service.AmortisasiBarangService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Repository
public class AmortisasiBarangImpl implements AmortisasiBarangService {
    @Autowired
    private AmortisasiBarangRepository amortisasiBarangRepository;

    @Override
    public AmortisasiBarang findOne(Integer id) {
        return null;
    }

    @Override
    public void delete(Integer id) {
        amortisasiBarangRepository.deleteById(id);
    }

    @Override
    public void save(AmortisasiBarang c) {
        amortisasiBarangRepository.save(c);
    }

    @Override
    public void saveAll(List<AmortisasiBarang> c) {
        amortisasiBarangRepository.saveAll(c);
    }

    @Override
    public List<AmortisasiBarang> findAmortisasiBarangByNoTransaksi(Integer noTransaksi) {
        return amortisasiBarangRepository.findAmortisasiBarangByNoTransaksi(noTransaksi);
    }


    @Override
    public List<AmortisasiBarang> getAll() {
        List<AmortisasiBarang> products = amortisasiBarangRepository.findAll();

        if (products.isEmpty()) {
            return null;
        }
        return products;
    }

}
