package id.co.dapenbi.core.repository;

import id.co.dapenbi.core.model.AmortisasiBarang;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AmortisasiBarangRepository extends JpaRepository<AmortisasiBarang, Integer> {
    List<AmortisasiBarang> findAll();
    List<AmortisasiBarang> findAmortisasiBarangByNoTransaksi(Integer noTransaksi);
}
