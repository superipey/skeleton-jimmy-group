package id.co.dapenbi.core.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "PENGADAAN_BARANG")
@EntityListeners(AuditingEntityListener.class)
public class PengadaanBarang {

    @Id
    @Column(name = "NO_TRANSAKSI")
    private Integer noTransaksi;

    @DateTimeFormat(pattern="yyyy-MM-dd")
    @Column(name = "TGL_TRANSAKSI")
    private Date tglTransaksi;

    @Column(name = "KETERANGAN")
    private String keterangan;

    @Column(name = "PEROLEHAN")
    private Integer perolehan;

    @Column(name = "LAMA_PEROLEHAN")
    private Integer lamaPerolehan;

    public Integer getNoTransaksi() {
        return noTransaksi;
    }

    public void setNoTransaksi(Integer noTransaksi) {
        this.noTransaksi = noTransaksi;
    }

    public Date getTglTransaksi() {
        return tglTransaksi;
    }

    public void setTglTransaksi(Date tglTransaksi) {
        this.tglTransaksi = tglTransaksi;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public Integer getPerolehan() {
        return perolehan;
    }

    public void setPerolehan(Integer perolehan) {
        this.perolehan = perolehan;
    }

    public Integer getLamaPerolehan() {
        return lamaPerolehan;
    }

    public void setLamaPerolehan(Integer lamaPerolehan) {
        this.lamaPerolehan = lamaPerolehan;
    }
}
