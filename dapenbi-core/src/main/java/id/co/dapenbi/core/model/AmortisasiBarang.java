package id.co.dapenbi.core.model;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "AMORTISASI_BARANG")
@EntityListeners(AuditingEntityListener.class)
public class AmortisasiBarang {

    @Column(name = "NO_TRANSAKSI")
    private Integer noTransaksi;

    @Id
    @Column(name = "PERIODE_AMORTISASI")
    private String periodeAmortisasi;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(name = "TGL_AMORTISASI")
    private Date tglAmortisasi;

    @Column(name = "SALDO_AWAL")
    private Integer saldoAwal;

    @Column(name = "AMORTISASI_BULANAN")
    private Integer amortisasiBulanan;

    @Column(name = "SALDO_AKHIR")
    private Integer saldoAkhir;

    public Integer getNoTransaksi() {
        return noTransaksi;
    }

    public void setNoTransaksi(Integer noTransaksi) {
        this.noTransaksi = noTransaksi;
    }

    public Date getTglAmortisasi() {
        return tglAmortisasi;
    }

    public void setTglAmortisasi(Date tglAmortisasi) {
        this.tglAmortisasi = tglAmortisasi;
    }

    public Integer getSaldoAwal() {
        return saldoAwal;
    }

    public void setSaldoAwal(Integer saldoAwal) {
        this.saldoAwal = saldoAwal;
    }

    public Integer getAmortisasiBulanan() {
        return amortisasiBulanan;
    }

    public void setAmortisasiBulanan(Integer amortisasiBulanan) {
        this.amortisasiBulanan = amortisasiBulanan;
    }

    public Integer getSaldoAkhir() {
        return saldoAkhir;
    }

    public void setSaldoAkhir(Integer saldoAkhir) {
        this.saldoAkhir = saldoAkhir;
    }

    public String getPeriodeAmortisasi() {
        return periodeAmortisasi;
    }

    public void setPeriodeAmortisasi(String periodeAmortisasi) {
        this.periodeAmortisasi = periodeAmortisasi;
    }
}
