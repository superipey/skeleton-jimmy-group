package id.co.dapenbi.core.service;

import id.co.dapenbi.core.model.AmortisasiBarang;
import id.co.dapenbi.core.model.PengadaanBarang;

import java.util.List;

public interface AmortisasiBarangService {
    AmortisasiBarang findOne(Integer id);
    void delete(Integer id);
    void save(AmortisasiBarang c);
    void saveAll(List<AmortisasiBarang> c);

    List<AmortisasiBarang> findAmortisasiBarangByNoTransaksi(Integer noTransaksi);

    List<AmortisasiBarang> getAll();
}
