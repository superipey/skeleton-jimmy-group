package id.co.dapenbi.core.repository;

import id.co.dapenbi.core.model.PengadaanBarang;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PengadaanBarangRepository extends JpaRepository<PengadaanBarang, Integer> {
    List<PengadaanBarang> findAll();
}
