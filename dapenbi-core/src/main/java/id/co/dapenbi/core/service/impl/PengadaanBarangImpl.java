package id.co.dapenbi.core.service.impl;

import com.fasterxml.jackson.annotation.JsonIgnore;
import id.co.dapenbi.core.model.AmortisasiBarang;
import id.co.dapenbi.core.model.PengadaanBarang;
import id.co.dapenbi.core.repository.PengadaanBarangRepository;
import id.co.dapenbi.core.service.PengadaanBarangService;
import org.hibernate.annotations.OnDelete;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import java.util.List;

@Service
@Repository
public class PengadaanBarangImpl implements PengadaanBarangService {
    @Autowired
    private PengadaanBarangRepository pengadaanBarangRepository;

    @Override
    public PengadaanBarang findOne(Integer id) {
        return pengadaanBarangRepository.getOne(id);
    }

    @Override
    public void delete(Integer id) {
        pengadaanBarangRepository.deleteById(id);
    }

    @Override
    public void save(PengadaanBarang c) {
        pengadaanBarangRepository.save(c);
    }

    @Override
    public List<PengadaanBarang> getAll() {
        List<PengadaanBarang> products = pengadaanBarangRepository.findAll();


        if (products.isEmpty()) {
            return null;
        }
        return products;
    }

}
