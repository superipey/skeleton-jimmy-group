package id.co.dapenbi.core.service;

import id.co.dapenbi.core.model.PengadaanBarang;

import java.util.List;

public interface PengadaanBarangService {
    PengadaanBarang findOne(Integer id);
    void delete(Integer id);
    void save(PengadaanBarang c);

    List<PengadaanBarang> getAll();
}
