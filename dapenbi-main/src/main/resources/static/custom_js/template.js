jQuery(document).ready(function () {
    $(document).on('show.bs.modal', '.modal', function (event) {
        var zIndex = 1040 + (10 * $('.modal:visible').length);
        $(this).css('z-index', zIndex);
        setTimeout(function () {
            $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
        }, 0);
    });

    $('#btnSignOut').attr("href", _baseUrl + "/logout");
    
    $('#cardBackground').css("background-image", "url(" + _baseUrl + "/metronic/media/misc/bg-1.jpg)");
    
    $('#currentPage').html(document.title);

    buildMenu();
    getUserIdentity();
});

function buildMenu(){
	$.get(API_BASE_URL + "/api/common/getUserMenu",
    {
        programName: PROGRAM_NAME,
        flagFunction: true,
        nip: _userLoggedIn
    }
    , function (response) {
        var toAppend = '';
        $.when(
    		buildFunctionMenu(response.data)
        ).done(function () {
            $.when(
            	buildParent(response.dataParent)
            ).done(function (){
            	buildFunctionWithParent(response.data)
            });
        });
    }, "json");
}

function buildFunctionMenu(data){
	$.each(data, function (index, item) {
    	if(item.menu.menuParent === null){
    		toAppend = '<li id="menu-' + item.menu.menuId + '" class="kt-menu__item kt-menu__item--submenu"><a href="' + _baseUrl + item.menu.menuUrl + '" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon ' +
            item.menu.menuIcon + '"></i><span class="kt-menu__link-text">' + item.menu.menuName + '</span></a></li>';
    		$("#sidebarMenu").append(toAppend);
    	} 
    });
}

function buildFunctionWithParent(data){
	$.each(data, function (index, item) {
    	if(item.menu.menuParent !== null){
    		toAppend = '<li class="kt-menu__item " aria-haspopup="true">' +
    	    '<a href="' + _baseUrl + item.menu.menuUrl + '" class="kt-menu__link ">' +
    	    '<i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>' +
    	    '<span class="kt-menu__link-text">' + item.menu.menuName + '</span>' +
    	    '</a></li>';
    		$('#ulMenu-' + item.menu.menuParent.menuId).append(toAppend);
    	} 
    });
}

function buildParent(dataParent){
	if(dataParent[0] !== null){
        $.each(dataParent, function (index, item) {
            toAppend = '<li id="menu-' + item.menuId + '" class="kt-menu__item kt-menu__item--submenu"><a href="javascript:void(0);" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon ' +
                item.menuIcon + '"></i><span class="kt-menu__link-text">' + item.menuName + '</span></a></li>';
            	if(item.menuParent !== null){
            		toAppend = '<li id="menu-' + item.menuParent.menuId + '" class="kt-menu__item kt-menu__item--submenu"><a href="javascript:void(0);" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon ' +
                    item.menuParent.menuIcon + '"></i><span class="kt-menu__link-text">' + item.menuParent.menuName + '</span></a></li>';
            	}
            if(!$('#menu-' + item.menuId).length){
                $.when(
                		appendMenuItem(item, toAppend)
                ).done(function () {
                    toAppend = '<div class="kt-menu__submenu " kt-hidden-height="840" style="">' +
                        '<span class="kt-menu__arrow"></span>' +
                        '<ul class="kt-menu__subnav" id="ulMenu-' + item.menuId + '">' +
                        '<li class="kt-menu__item  kt-menu__item--parent" aria-haspopup="true">' +
                        '<span class="kt-menu__link"><span class="kt-menu__link-text">' + item.menuName + '</span></span>' +
                        '</li></ul></div>';
                    
                    if(item.menuParent != null){
                    	toAppend = '<div class="kt-menu__submenu " kt-hidden-height="840" style="">' +
                        '<span class="kt-menu__arrow"></span>' +
                        '<ul class="kt-menu__subnav" id="ulMenu-' + item.menuParent.menuId + '">' +
                        '<li class="kt-menu__item  kt-menu__item--parent" aria-haspopup="true">' +
                        '<span class="kt-menu__link"><span class="kt-menu__link-text">' + item.menuParent.menuName + '</span></span>' +
                        '</li></ul></div>';
                    	
                    	$('#menu-' + item.menuParent.menuId).append(toAppend);
                    } else {
                    	$('#menu-' + item.menuId).append(toAppend);
                    }
                    
                    $.when(
                    		appendRightArrow(item)
                    ).done(function(){
                    	if(item.menuParent !== null){
                    		//console.log(item.menuParent.menuName);
                    		toAppend = '<li id="menu-' + item.menuId + '" class="kt-menu__item kt-menu__item--submenu"><a href="javascript:void(0);" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon ' +
                            item.menuIcon + '"></i><span class="kt-menu__link-text">' + item.menuName + '</span></a></li>';
                    		
                    		$.when(
                    			$('#ulMenu-' + item.menuParent.menuId).append(toAppend)
                    		).done(function(){
                    			toAppend = '<div class="kt-menu__submenu " kt-hidden-height="840" style="">' +
    	                        '<span class="kt-menu__arrow"></span>' +
    	                        '<ul class="kt-menu__subnav" id="ulMenu-' + item.menuId + '">' +
    	                        '<li class="kt-menu__item  kt-menu__item--parent" aria-haspopup="true">' +
    	                        '<span class="kt-menu__link"><span class="kt-menu__link-text">' + item.menuName + '</span></span>' +
    	                        '</li></ul></div>';
                    			$('#menu-' + item.menuId).append(toAppend);
                    			$('#menu-' + item.menuId).find('a').append('<i class="kt-menu__ver-arrow la la-angle-right"></i>');
                    		});
	                    }
                    })
                });
            }
        });
	} 
}

function getUserIdentity() {
    $.get(API_BASE_URL + "/api/common/getUserIdentity", {
        nip:_userLoggedIn
    }, function (data) {    	
    	var name = data.name;
    	var nip = data.nip;
    	
        $('#lblUserName').html(name);
        $('#cardUserName').html(name + " - " + nip);

        var words = name.split(" ");
        var char = '';
        for (i = 0; i < words.length; i++) {
            char += words[i].charAt(0);
        }
        $('#lblUserInitial').html(char);
        $('#cardUserInitial').html(char);
    });
}

function appendMenuItem(item, toAppend){
		$("#sidebarMenu").append(toAppend);
		
}

function appendRightArrow(item){
	if(item.menuParent !== null)
		$('#menu-' + item.menuParent.menuId).find('a').append('<i class="kt-menu__ver-arrow la la-angle-right"></i>')
	else
		$('#menu-' + item.menuId).find('a').append('<i class="kt-menu__ver-arrow la la-angle-right"></i>')
}