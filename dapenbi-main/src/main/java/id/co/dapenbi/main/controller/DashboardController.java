package id.co.dapenbi.main.controller;

import id.co.dapenbi.core.model.AmortisasiBarang;
import id.co.dapenbi.core.model.PengadaanBarang;
import id.co.dapenbi.core.service.AmortisasiBarangService;
import id.co.dapenbi.core.service.PengadaanBarangService;
import id.co.dapenbi.core.service.impl.AmortisasiBarangImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.parameters.P;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
public class DashboardController {
    @Autowired
    PengadaanBarangService pengadaanBarangService;

    @Autowired
    AmortisasiBarangService amortitasiBarangService;

    @GetMapping("/dashboard")
    public String dashboard() {
        return "dashboard";
    }

    @GetMapping("/bsc/amortisasi")
    public String amortisasi() {
        return "amortisasi";
    }

    @PostMapping("bsc/amortisasi")
    public ResponseEntity<Object> resultAmortisasi(@RequestBody PengadaanBarang request) {
        request.setNoTransaksi(111);
        pengadaanBarangService.save(request);

        // Mengambil Perolehan
        Integer perolehan = request.getPerolehan();
        Integer lamaPerolehan = request.getLamaPerolehan();

        // Mengambil Potongan
        Integer amortisasiBulanan = request.getPerolehan() / lamaPerolehan;

        System.out.println("LAMA PEROLEHAN " + lamaPerolehan);

        Integer i = 0;
        for (i = 0; i < lamaPerolehan; i++) {
            AmortisasiBarang amortisasiBarang = new AmortisasiBarang();
            amortisasiBarang.setNoTransaksi(111);
            amortisasiBarang.setTglAmortisasi(request.getTglTransaksi());

            amortisasiBarang.setAmortisasiBulanan(amortisasiBulanan);

            String l = "0" + (i+1);
            if ((i+1) > 9) l = "" + (i+1);
            amortisasiBarang.setPeriodeAmortisasi(l + "2018");

            Integer saldoAwal = perolehan - (i * amortisasiBulanan);
            Integer saldoAkhir = (perolehan - (i * amortisasiBulanan)) - amortisasiBulanan;

            amortisasiBarang.setSaldoAwal(saldoAwal);
            amortisasiBarang.setSaldoAkhir(saldoAkhir);

            amortitasiBarangService.save(amortisasiBarang);
        }

        // Mengambil Data by ID
        List<AmortisasiBarang> a = amortitasiBarangService.findAmortisasiBarangByNoTransaksi(111);

        return ResponseEntity.status(HttpStatus.CREATED).body(a);
    }
}
